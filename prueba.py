import requests


def hacer_peticion():

    url = "http://www.mocky.io/v2/5d37a71e3100006925b079a2"

    resp = requests.get(url)

    # print("resp.content:", resp.content)
    usuarios = resp.json()

    # print("resp.json:", type(usuarios))

    for usuario in usuarios:
        nombre = usuario.get('name')
        apellido = usuario.get('last_name')
        print("Su nombre es {} y su apellido es {}".format(nombre, apellido))


if __name__ == '__main__':
    hacer_peticion()
